import math

my_function = input('Функция, которую вы хотите посчитать: ')

if my_function in ['G', 'F', 'Y']:
    pass
else:
    exit('Неправильное название функции')

try:
    x = float(input('Введите значение переменной x для функции : '))
    a = float(input('Введите значение переменной a для функции : '))
except ValueError:
    exit('Вы ввели неверное значение ("ValueError")')

if my_function == 'G':
    try:
        G = (3 * (3 * a ** 2 - 12 * a * x + 4 * x ** 2)) / (54 * a ** 2 + 87 * a * x + 35 * x ** 2)
    except ZeroDivisionError:
        exit('Zero Division Error')
    print('Function G = ', G)

if my_function == 'F':
    try:
        F = math.cosh(4 * a ** 2 - x ** 2)
    except OverflowError:
        exit('Overflow Error')
    print('Function F = ', F)

if my_function == 'Y':
    try:
        Y = math.asin(2 * a ** 2 - 23 * a * x + 45 * x ** 2)
    except:
        exit('Value Error')
    print('Function Y = ', Y)

