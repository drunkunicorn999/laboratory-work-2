import matplotlib.pyplot as plt
import math

while True:
    my_function = input('Функция, которую вы хотите посчитать: ')
    if my_function in ['g', 'f', 'y']:
        pass
    else:
        exit('Неправильное название функции')

    x_list = []
    y_list = []

    try:
        x = float(input('Введите значение переменной x для функции: '))
        a = float(input('Введите значение переменной a для функции: '))
        x_max = float(input('Введите максимальное значение x: '))
        x_step = float(input('Введите количество шагов: '))
    except ValueError:
        print('Вы ввели неверное значение ("ValueError")')
        break

    if my_function == 'g':
        while x < x_max:
            try:
                g = (3 * (3 * a ** 2 - 12 * a * x + 4 * x ** 2)) / (54 * a ** 2 + 87 * a * x + 35 * x ** 2)
                x_list.append(x), y_list.append(g)
                x += x_step
            except ZeroDivisionError:
                x_list.append(x), y_list.append(None)
                print('Вы поделили на ноль!')
                break

    if my_function == 'f':
        while x < x_max:
            try:
                f = math.cosh(4 * a**2 - x**2)
                x_list.append(x), y_list.append(f)
                x += x_step
            except OverflowError or ValueError:
                x_list.append(x), y_list.append(None)
                break

    if my_function == 'y':
        if x >= -1:
            while x < x_max:
                try:
                    y = math.asin(2 * a ** 2 - 23 * a * x + 45 * x ** 2)
                    x_list.append(x), y_list.append(y)
                    x += x_step
                except ValueError:
                    x_list.append(x), y_list.append(None)
                    break

    print(x_list)
    print(y_list)
    plt.plot(x_list, y_list, 'k')
    plt.axis('tight')
    plt.show()

    re = input('Вы хотите выйти? yes/no ')
    if re == 'yes':
        break
    elif re == 'no':
        pass
    else:
        exit('Вы ввели неверное значение')
